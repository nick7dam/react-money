# react-currency-input-mask

> ReactJS simple currency input field

[![NPM](https://img.shields.io/npm/v/react-money.svg)](https://www.npmjs.com/package/react-simple-calendar) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm i react-currency-input-mask --save
```
## GitLab
https://gitlab.com/damjan89/react-money
## Usage
React >= 16.9.0
```tsx
import * as React from 'react'

import ReactMoneyComponent from 'react-currency-input-mask'
import 'react-currency-input-mask/src/assets/style.css'
class IndexComponent extends React.Component {
   constructor(props:any) {
       super(props);
       this.state = {
         price: 2.2,
         config:  {
           prefix: '$',
           suffix: '%',
           precision: 2,
           className: 'yourClassName',
           decimalSeparator: ',',
         }
       }
     }
/*
  * Name: changePrice
  * Description: This function is called when user change input value
  * Parameters: price(changed Price)
  * author: Nick Dam
  * */
  changePrice(price:number){
      this.setState({price: price})
  }
  render () {
    return (
        <div>
          <p>{this.state.price}</p>
          <ReactMoneyComponent 
              value={this.state.price} 
              config={this.state.config} 
              onChange={(e:any) => this.changePrice(e)}><
          /ReactMoneyComponent>
        </div>
    )
  }
}
```
## Styling Component
```
In this.state.config add your class name!

To style input field:
.yourClassName .reactMoneyInput {
    //yourStyle
}

To style prefix:
.yourClassName .reactMoneyInputGroupPrepend {
    //yourStyle
}

To style suffix:
.yourClassName .reactMoneyInputGroupAppend {
    //yourStyle
}
```
## License

MIT © [Nick Dam](https://gitlab.com/damjan89)
